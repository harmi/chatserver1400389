/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatserver;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 * @author Matti
 */
public class InputHandler implements ChatObserver {

    private String userIP = "";
    private String username = "";
    private String userRequest = "";
    private Socket socket;
    PrintWriter out;
    History history;

    public InputHandler(String user, Socket socket) throws IOException {
        this.out = new PrintWriter(socket.getOutputStream());
        this.history = History.getHistory();
        this.userIP = user;
        this.socket = socket;

        this.history.register(this);
      //  this.history.getChatHistory(10, this);

    }

    public void inputAnalyzer(String userInput) throws IOException {
        System.out.println("Input analyzer received input " + userInput);
        if (userInput.startsWith("/name")) {
            setUsername(userInput);
        } else if (userInput.startsWith("/whois")) {
            //commandSplitter(userInput);
            this.out.println("Functionality not in place.");
            this.out.flush();
        } else if (userInput.startsWith("/help")) {
            this.out.println("Type /name X to change name");
            this.out.println("Type /whois x to get info on user");
            this.out.flush();
        } else {
            System.out.println("Sending input " + userInput + " to inputCompiler");
            inputCompiler(userInput);
        }
    }

    public void inputCompiler(String userInput) {
        System.out.println("Input " + userInput + " received in inputCompiler");
        System.out.println("Username lenght is " + this.username.length());
        if (this.username.length() == 0) {
            userInput = this.userIP + " wrote: " + userInput;
            System.out.println("Sending line " + userInput + " to history with userIp");
            this.history.addLine(userInput);
        } else if (this.username.length() > 0) {
            userInput = this.username + " wrote: " + userInput;
            System.out.println("Sending line " + userInput + " to history with username");
            this.history.addLine(userInput);
        }
    }

    public void commandSplitter(String userInput) {
        String[] parts = userInput.split(" ");
        if (parts.length != 2) {
            out.println("incorrect command");
        } else {
            this.userRequest = parts[1];
        }
    }

    @Override
    public void chatEntry(String line) {
        out.flush();
        out.println(line);
        System.out.println("Flushing line " + line + " to users.");
        out.flush();
    }

    public void newClientConnected(String line) {
        this.history.addLine(line);
    }
    
    public void sendToHistory(String line) {
        this.history.addLine(line);
    }
    
    public void setUsername(String userInput) {
        commandSplitter(userInput);
        this.username = this.userRequest;

    }
    
    public void setUsernameFromIntro(String userInput) {
        this.username = userInput;
    }
    
    public void printHistory() {
        this.history.getChatHistory(10, this);
    }

}
