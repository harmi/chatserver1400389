/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatserver;

import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author Matti
 */
public class ChatServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        Socket socket;
        //creating a new server socket, with #5 backlog
        SocketMaker serverS = new SocketMaker(100);

        //incoming connection loop
        while (true) {
            //waiting for accept method to return a socket for the new connection
            socket = serverS.connectionReceiver();
            //creating a new connection
            // Connection connection = new Connection(socket) ;
            Connection connection = new Connection(socket) ;
            //creating a new thread and passing the connection 
            Thread t = new Thread(connection);
            //staring thread
            t.start();

        }

    }

}
